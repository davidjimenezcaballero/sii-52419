// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Plano.h"
#include "Vector2D.h"

//los jugadores son de tipo raqueta, es decir, implementamos la clase raqueta (jugador)

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;
	//estan incluidos x1,2 e y1,2 de plano.h para el movimiento de jugadores
	//por ello no me creo un vector posicion de cada jugador
	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
};
