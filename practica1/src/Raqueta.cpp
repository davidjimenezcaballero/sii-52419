// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{	
	//movemos a todo el jugador (tanto en x1 como en x2 se mueve con la 		misma velocidad (velocidad.x). Idem para y1 e y2)
	x1=x1+velocidad.x*t;
	x2=x2+velocidad.x*t;
	y1=y1+velocidad.y*t;
	y2=y2+velocidad.y*t;
}
